const request = require('request');
CONFIG = require('./config');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
const help = `
*************** HELP ***************
*   COMMAND    ****** DESCRIPTION  *
************************************
*  help     - display this help    *
*  exit     - exit this app        *
*  login    - force login          *
************************************\n`;

function login() {
    request.post({
        //method: 'GET',
        headers: {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0',
            'content-type': 'application/x-www-form-urlencoded',
        },
        url: 'https://wifi.sspbrno.cz/login.html',
        //body: `buttonClicked=0&err_flag=0&err_msg=&info_flag=0&info_msg=&redirect_url=&username=${CONFIG.username}&password=${CONFIG.password}`,
        form: {
            buttonClicked: 4,
            err_flag: 0,
            err_msg: '',
            info_flag: 0,
            info_msg: '',
            redirect_url: 'google.com',
            username: CONFIG.auth.username,
            password: CONFIG.auth.password,
            Submit: 'Submit',
        },
        //encoding: 'binary',
    }, function (err, response, body) {
        if (err) {
            return console.error('error:', err); // Print the error if one occurred
        }
        if (response) {
            if (response.statusCode == 200) {
                console.log('Login should be successfull!');
            } else {
                console.log('Trying again ...');
                login();
            }
            //console.log('statusCode:', response.statusCode); // Print the response status code if a response was received        
            //console.log(response);
        }
    });
}

// Get process.stdin as the standard input object.
var standard_input = process.stdin;

// Set input character encoding.
standard_input.setEncoding('utf-8');

// Prompt user to input data in console.
console.log("Please input text in command line.");

// When user input data and click enter key.
standard_input.on('data', (data) => {

    switch (data.trim()) {
        case 'exit':
            console.log('I am exiting now ...')
            process.exit();
            break;

        case 'login':
            console.log('You want to login!');
            login();
            break;

        case 'help':
            console.log(help);
            break;
        
        case '':
            break;

        default:
            console.log(`Unsupported command: '${data.trim()}'`);
            break;
    }
    process.stdout.write(`> `);
});

login();
process.stdout.write(help);
process.stdout.write(`\n> `);