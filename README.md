# # SSPBrno WiFi autologin

## Installation
### Linux
#### Debian based
```sudo apt install node npm -y && git clone https://gitlab.com/matuska.lukas/sspbrno-wifi-autologin.git && cd sspbrno-wifi-autologin && npm i && node app.js```
#### Arch linux based
```sudo pacman -Sy node npm  && git clone https://gitlab.com/matuska.lukas/sspbrno-wifi-autologin.git && cd sspbrno-wifi-autologin && npm i && node app.js```
### Windows
Not supported, but you can try it...
- Download and install nodejs (& git)
- ```git clone https://gitlab.com/matuska.lukas/sspbrno-wifi-autologin.git ```
- ```cd sspbrno-wifi-autologin```
- ```npm i```

## Running
### Config file
1. Copy sample config file
```cp config.sample.js config.js```
2. Edit config file (Entry login credentials)
### Log me in !
```node app.js```

### Force login
Command: ```login```

### Exit
Command: ```exit```